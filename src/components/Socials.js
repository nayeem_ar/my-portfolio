import React from "react";

// import icons
import { social } from "../data";
const shoot = (name) => {
  console.log("item", name);
  if (name === "linkedIn") {
    window.open("https://www.linkedin.com/in/atiar-rahman-nayeem/");
  } else {
    window.open("https://gitlab.com/nayeem_ar");
  }
};

const Socials = () => {
  return (
    <ul className="flex space-x-6 ">
      {social.map((item, index) => {
        return (
          <li
            className="flex justify-center items-center text-accent"
            key={index}
          >
            <div
              onClick={() => shoot(item.name)}
              className="text-base cursor-pointer"
              href={item.href}
            >
              {item.icon}
            </div>
          </li>
        );
      })}
    </ul>
  );
};

export default Socials;
